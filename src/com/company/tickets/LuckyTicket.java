package com.company.tickets;

import com.company.exceptions.InvalidDigitPositionException;
import com.company.exceptions.InvalidTicketNumberException;

public interface LuckyTicket {

    public boolean isLucky() throws InvalidDigitPositionException, InvalidTicketNumberException;
    public int getLuckySum() throws InvalidDigitPositionException, InvalidTicketNumberException;
    public String getLuckyString() throws InvalidTicketNumberException, InvalidDigitPositionException;

}
