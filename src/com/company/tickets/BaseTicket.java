package com.company.tickets;

import com.company.exceptions.InvalidDigitPositionException;
import com.company.exceptions.InvalidTicketNumberException;

public abstract class BaseTicket implements LuckyTicket {

    protected Integer number;

    public BaseTicket(Integer number) throws InvalidTicketNumberException {
        setNumber(number);
    }

    protected void validateNumber(Integer number) throws InvalidTicketNumberException {
        if ((number < 0) || (number > 999999) || (number == null)) {
            throw new InvalidTicketNumberException(number);
        }
    }

    protected void validateDigitPos(int digitPos) throws InvalidDigitPositionException {
        if ((digitPos < 0) || (digitPos > 5)) {
            throw new InvalidDigitPositionException(digitPos);
        }
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) throws InvalidTicketNumberException {
        validateNumber(number);
        this.number = number;
    }

    /*
    Выделение разряда из номера билета
    digitPos - позиция разряда в номере
    0 - единицы
    1 - десятки
    2 - сотни
    3 - тысячи
    4 - десятки тысяч
    5 - сотни тысяч
     */
    public int getDigit(int digitPos) throws InvalidDigitPositionException, InvalidTicketNumberException {
        validateNumber(number);
        validateDigitPos(digitPos);
        return (number / (int)Math.pow(10, digitPos)) % 10;
    }

}
