package com.company.tickets;

import com.company.exceptions.InvalidDigitPositionException;
import com.company.exceptions.InvalidTicketNumberException;

public class MoscowTicket extends BaseTicket implements LuckyTicket {

    public MoscowTicket(Integer number) throws InvalidTicketNumberException {
        super(number);
    }

    public int getLeftSum() throws InvalidDigitPositionException, InvalidTicketNumberException {
        return getDigit(5) + getDigit(4) + getDigit(3);
    }

    public int getRightSum() throws InvalidDigitPositionException, InvalidTicketNumberException {
        return getDigit(2) + getDigit(1) + getDigit(0);
    }

    @Override
    public boolean isLucky() throws InvalidDigitPositionException, InvalidTicketNumberException {
        validateNumber(number);
        return getLeftSum() == getRightSum();
    }

    @Override
    public int getLuckySum() throws InvalidDigitPositionException, InvalidTicketNumberException {
        return getLeftSum();
    }

    @Override
    public String getLuckyString() throws InvalidTicketNumberException, InvalidDigitPositionException {
        String result = String.format("Счастливый билет (по-московски) №%06d (%d+%d+%d = %d+%d+%d = %d)",
                getNumber(),
                getDigit(5),
                getDigit(4),
                getDigit(3),
                getDigit(2),
                getDigit(1),
                getDigit(0),
                getLuckySum());
        return result;
    }
}
