package com.company.tickets;

import com.company.exceptions.InvalidDigitPositionException;
import com.company.exceptions.InvalidTicketNumberException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Ticket {

    private Integer number;
    private Map<LuckyType, BaseTicket> luckyMap;

    public Ticket(Integer number) throws InvalidTicketNumberException {
        luckyMap = new HashMap<LuckyType, BaseTicket>();
        luckyMap.put(LuckyType.LuckyMoscow, new MoscowTicket(number));
        luckyMap.put(LuckyType.LuckyPeter, new PeterTicket(number));
        this.number = number;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) throws InvalidTicketNumberException {
        for (Map.Entry entry : luckyMap.entrySet()) {
            BaseTicket ticket = (BaseTicket)entry.getValue();
            ticket.setNumber(number);
        }
        this.number = number;
    }

    public int getDigit(int digitPos) throws InvalidTicketNumberException, InvalidDigitPositionException {
        BaseTicket ticket = luckyMap.get(LuckyType.LuckyMoscow);
        return ticket.getDigit(digitPos);
    }

    public ArrayList<LuckyType> getLuckySet() throws InvalidTicketNumberException, InvalidDigitPositionException {
        ArrayList<LuckyType> result = new ArrayList<LuckyType>();
        LuckyType[] types = LuckyType.values();
        for (LuckyType type : types) {
            if (luckyMap.get(type).isLucky()) {
                result.add(type);
            }
        }
        return result;
    }

    public boolean isLucky(LuckyType luckyType) throws InvalidTicketNumberException, InvalidDigitPositionException {
        return luckyMap.get(luckyType).isLucky();
    }

    public int getLuckySum(LuckyType luckyType) throws InvalidTicketNumberException, InvalidDigitPositionException {
        return luckyMap.get(luckyType).getLuckySum();
    }

    public String getLuckyString() throws InvalidTicketNumberException, InvalidDigitPositionException {
        StringBuilder result = new StringBuilder();
        for (Map.Entry entry : luckyMap.entrySet()) {
            BaseTicket ticket = (BaseTicket)entry.getValue();
            if (ticket.isLucky()) {
                result.append(ticket.getLuckyString());
                result.append("\n");
            }
        }
        if (result.length() != 0) {
            result.delete(result.lastIndexOf("\n"), result.length());
        }
        return result.toString();
    }

}
