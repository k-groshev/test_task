package com.company.exceptions;

public class InvalidDigitPositionException extends Exception {

    private int digitPos;

    public InvalidDigitPositionException(int digitPos) {
        super();
        this.digitPos = digitPos;
    }

    public String toString() {
        String result = "Invalid digit position: " + Integer.toString(digitPos)
                + ". The digit position must be in 0...5";
        return result;
    }

}
