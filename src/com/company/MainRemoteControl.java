package com.company;

import com.company.commands.*;

import java.util.ArrayList;
import java.util.Scanner;

public class MainRemoteControl {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        RemoteControl remoteControl = new RemoteControl();
        Light light = new Light(false);
        Door door = new Door(false);
        boolean isExit = false;
        remoteControl.addCommand(new LightOnCommand("Свет вкл.", "Включить освещение", light));
        remoteControl.addCommand(new LightOffCommand("Свет выкл.", "Выключить освещение", light));
        remoteControl.addCommand(new DoorOpenCommand("Дверь откр.", "Открыть дверь", door));
        remoteControl.addCommand(new DoorCloseCommand("Дверь закр.", "Закрыть дверь", door));
        //Добавляем другие команды
        do {
            //Вывод текущего состояния объектов
            System.out.println("Текущее состояние объектов:");
            String state = "";
            if (light.isSwitchedOn()) {
                state = "включено";
            } else {
                state = "выключено";
            }
            System.out.println("Освещение: " + state);
            if (door.isOpened()) {
                state = "открыта";
            } else {
                state = "закрыта";
            }
            System.out.println("Дверь: " + state);
            System.out.println();
            System.out.println("Введите строку, соответствующую выбранной команде:");
            ArrayList<String> inputs = new ArrayList<String>();
            for (int i = 0; i < remoteControl.getCommandsAmount(); i++) {
                BaseCommand command = remoteControl.getCommand(i);
                System.out.printf("%d - \"%s\" (%s)\n", i, command.getName(), command.getDescription());
                inputs.add(Integer.toString(i));
            }
            inputs.add("exit");
            System.out.print("exit - выход из программы\nВаш выбор: > ");
            if (scanner.hasNextLine()) {
                String str = scanner.nextLine();
                if (inputs.contains(str)) {
                    if (str.equals("exit")) {
                        isExit = true;
                    } else {
                        int selectedCommand = Integer.parseInt(str);
                        remoteControl.getCommand(selectedCommand).execute();
                        System.out.println("Выполнено.\n");
                    }
                } else {
                    System.out.println("\nОшибка ввода - указана строка, не предусмотренная меню.\n");
                }
            }

        } while (!isExit);
    }

}
