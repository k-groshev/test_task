package com.company.commands;

public class DoorCloseCommand extends BaseCommand {

    private Door door;

    public DoorCloseCommand(Door door) {
        super();
        setDoor(door);
    }

    public DoorCloseCommand(String name, String description, Door door) {
        super(name, description);
        setDoor(door);
    }

    public Door getDoor() {
        return door;
    }

    public void setDoor(Door door) {
        this.door = door;
    }

    @Override
    public void execute() {
        door.close();
    }

}
