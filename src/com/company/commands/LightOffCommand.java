package com.company.commands;

public class LightOffCommand extends BaseCommand{

    private Light light;

    public LightOffCommand(Light light) {
        super();
        setLight(light);
    }

    public LightOffCommand(String name, String description, Light light) {
        super(name, description);
        setLight(light);
    }

    public Light getLight() {
        return light;
    }

    public void setLight(Light light) {
        this.light = light;
    }

    @Override
    public void execute() {
        light.switchOff();
    }

}
