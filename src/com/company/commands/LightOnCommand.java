package com.company.commands;

public class LightOnCommand extends BaseCommand {

    private Light light;

    public LightOnCommand(Light light) {
        super();
        setLight(light);
    }

    public LightOnCommand(String name, String description, Light light) {
        super(name, description);
        setLight(light);
    }

    public Light getLight() {
        return light;
    }

    public void setLight(Light light) {
        this.light = light;
    }

    @Override
    public void execute() {
        light.switchOn();
    }

}
