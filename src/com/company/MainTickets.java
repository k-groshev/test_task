package com.company;

import com.company.exceptions.InvalidDigitPositionException;
import com.company.exceptions.InvalidTicketNumberException;
import com.company.tickets.LuckyType;
import com.company.tickets.Ticket;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainTickets {

    public static void main(String[] args) {
        Map<Integer, Ticket> luckyTickets = new HashMap<Integer, Ticket>();
        Map<LuckyType, Integer> luckyTypeHits = new HashMap<LuckyType, Integer>();
        LuckyType[] types = LuckyType.values();
        for (LuckyType type : types) {
            luckyTypeHits.put(type, 0);
        }
        StringBuilder builder = new StringBuilder();
        System.out.printf("Вывод всех счастливых билетов и подсчет их количества.%n%n");
        try {
            for (int i = 0; i < 1000000; i++) {
                Ticket ticket = new Ticket(i);
                ArrayList<LuckyType> luckyTypes = ticket.getLuckySet();
                if(luckyTypes.size() != 0) {
                    luckyTickets.put(i, ticket);
                }
            }
            for (Map.Entry entry : luckyTickets.entrySet()) {
                Ticket ticket = (Ticket)entry.getValue();
                ArrayList<LuckyType> luckyTypes = ticket.getLuckySet();
                for (LuckyType type : luckyTypes) {
                    int hits = luckyTypeHits.get(type);
                    hits++;
                    luckyTypeHits.put(type, hits);
                }
                builder.append(ticket.getLuckyString() + "\n");
            }
        } catch (InvalidTicketNumberException e) {
            e.printStackTrace();
        } catch (InvalidDigitPositionException e) {
            e.printStackTrace();
        }
        System.out.println(builder);
        System.out.println();
        for (Map.Entry entry : luckyTypeHits.entrySet()) {
            LuckyType type = (LuckyType)entry.getKey();
            String str;
            switch (type) {
                case LuckyMoscow:
                    str = "(по-московски)";
                    break;
                case LuckyPeter:
                    str = "(по-питерски)";
                    break;
                default:
                    str = "";
                    break;
            }
            int hits = (Integer)entry.getValue();
            System.out.printf("Количество счастливых билетов %s: %d%n", str, hits);
        }
    }
}
